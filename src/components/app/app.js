import React, { Component } from "react";

import Header from "../header";
import RandomPlanet from "../random-planet";
import ErrorButton from "../error-button";
import ErrorBoundry from "../error-boundry";
import ErrorIndicator from "../error-indicator";
import {
  PersonDetails,
  PlanetDetails,
  StarshipDetails,
  PersonList,
  PlanetList,
  StarshipList,
} from "../sw-components/";
import SwapiService from "../../services/swapi-service";
import { SwapiServicePorvider } from "../swapi-service-context";

import "./app.css";

export default class App extends Component {
  swapiService = new SwapiService();

  state = {
    hasError: false,
  };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      return <ErrorIndicator />;
    }

    return (
      <ErrorBoundry>
        <SwapiServicePorvider value={this.swapiService}>
          <div className="app-wrapper">
            <Header />
            <RandomPlanet />

            {/* <div className="row mb2">
              <ErrorButton />
            </div> */}

            <PersonList />
            <PlanetList />
            <StarshipList />

            <PersonDetails itemId={11} />
            <PlanetDetails itemId={11} />
            <StarshipDetails itemId={11} />
          </div>
        </SwapiServicePorvider>
      </ErrorBoundry>
    );
  }
}
