import React, { Component } from "react";

import "./item-details.css";
import Spinner from "../spinner";
import ErrorButton from "../error-button";

export const Record = ({ item, field, label }) => {
  return (
    <li className="list-group-item">
      <span className="term">{label}</span>
      <span>{item[field]}</span>
    </li>
  );
}

export default class ItemDetails extends Component {
  state = {
    item: null,
    loading: true,
    image: null,
  };

  componentDidMount() {
    this.updateItem();
  }

  componentDidUpdate(prevProps) {
    if (this.props.itemId !== prevProps.itemId) {
      this.updateItem();
    }
  }

  updateItem() {
    const { itemId, getData, getImageUrl } = this.props;
    this.setState({ loading: true });
    if (!itemId) {
      return;
    }

    getData(itemId).then((item) => {
      console.log(item)
      this.setState({ item, loading: false, image: getImageUrl(item) });
    });
  }

  render() {
    console.log(this.state)
    if (!this.state.item) {
      return <span>Select a Item from a list</span>;
    }

    if (this.state.loading) {
      return <Spinner />
    }

    const { item, image } = this.state;
    const { name } = item;

    return (
      <div className="item-details card">
        <img
          className="item-image"
          src={image}
          alt="item"
        />

        <div className="card-body">
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">
            {
              React.Children.map(this.props.children, (child) => {
                return React.cloneElement(child, { item })
              })
            }
          </ul>
        </div>
        <ErrorButton />
      </div>
    );
  }
}
