import React from 'react';

const { Provider : SwapiServicePorvider, Consumer: SwapiServiceConsumer } = React.createContext();

export {
  SwapiServicePorvider,
  SwapiServiceConsumer
};
