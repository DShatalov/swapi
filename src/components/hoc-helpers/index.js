import withData from './with-data';
import withSwapiSerivce from './with-swapi-service';

export {
  withData,
  withSwapiSerivce
};
