import React, { Component } from "react";
import ItemList from "../item-list";
import ItemDetails from "../item-details";
import ErrorIndicator from "../error-indicator";
import Row from "../row";
import ErrorBoundry from "../error-boundry";

import "./people-page.css";
import SwapiService from "../../services/swapi-service";
import {Record} from "../item-details/item-details";


export default class PeoplePage extends Component {

  swapiService = new SwapiService();

  state = {
    selectedItem: null
  };

  onItemSelected = (selectedItem) => {
    this.setState({
      selectedItem,
    });
  };

  render() {

    if (this.state.hasError) {
      return <ErrorIndicator />;
    }

    const itemList = (
      <ItemList
        onItemSelected={this.onItemSelected}
        getData={this.swapiService.getAllPeople}>
        {(item) => `${item.name} (${item.gender})`}
      </ItemList>
    );

    const itemDetails = (
      <ItemDetails
        itemId={this.state.selectedItem}
        getData={this.swapiService.getPerson}
        getImageUrl={this.swapiService.getPersonImage}
      >
        <Record field="gender" label="Gender"/>
        <Record field="eyeColor" label="Eye Color"/>
      </ItemDetails>
    );

    return (
      <ErrorBoundry>
        <Row left={itemList} right={itemDetails} />
      </ErrorBoundry>
    );
  }
}
