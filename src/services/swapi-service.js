export default class SwapiService {
  _apiBase = "https://swapi.dev/api";
  _imgBase = "https://starwars-visualguide.com/assets/img"

  getResources = async (url)  => {
    const res = await fetch(`${this._apiBase}${url}`);

    if (!res.ok) {
      throw new Error(`Couldn not fetch data ${url}`);
    }

    const data = await res.json();
    return data;
  }

  getPersonImage = ({ id }) => {
    return `${this._imgBase}/characters/${id}.jpg`
  }

  getPlanetImage = ({ id }) => {
    return `${this._imgBase}/planets/${id}.jpg`
  }

  getStarshipImage = ({ id }) => {
    return `${this._imgBase}/starships/${id}.jpg`
  }

  getAllPeople = async () => {
    const res = await this.getResources(`/people/`);
    return res.results.map(this._transformPerson);
  }

  getPerson = async (id) => {
    const person = await this.getResources(`/people/${id}`);
    return this._transformPerson(person)
  }

  getAllPlanets = async () => {
    const res = await this.getResources(`/planets/`);
    return res.results.map(this._transformPlanet);
  }

  getPlanet = async (id) => {
    const planet = await this.getResources(`/planets/${id}`);
    return this._transformPlanet(planet);
  }

  getAllStarships = async () => {
    const res = await this.getResources(`/starships/`);
    return res.results.map(this._transformStarships);
  }

  getStarship = async (id) => {
    const starship = await this.getResources(`/starships/${id}`);
    return this._transformStarships(starship)
  }

  _extractId(item) {
    const idRegExp = /\/([0-9]*)\/$/;
    return item.url.match(idRegExp)[1];
  }

  _transformPlanet = (planet) => {
    const id = this._extractId(planet);

    return {
      id,
      name: planet.name,
      population: planet.population,
      rotationPeriod: planet.rotation_period,
      diameter: planet.diameter,
    };
  }

  _transformStarships = (starship) => {
    const id = this._extractId(starship);

    return {
      id,
      name: starship.name,
      model: starship.model,
      manufacturer: starship.manufacturer,
      constInCredits: starship.constInCredits,
      length: starship.length,
      crew: starship.crew,
      passengers: starship.passengers,
      cargoCapacity: starship.cargoCapacity
    };
  }

  _transformPerson = (person) => {
    const id = this._extractId(person);

    return {
      id,
      name: person.name,
      gender: person.gender,
      birthYear: person.birthYear,
      eyeColor: person.eyeColor
    };
  }
}
